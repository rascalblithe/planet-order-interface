import { clone } from 'lodash'
import { PlanetOrder, ClipTool, Polygon } from './generalModels'

export class TimahOrder {
  orderName: string
  imageId: string
  imageDate: string
  clipPolygons: Polygon[]
  clipArea: number

  constructor(params: any) {
    this.orderName = params.orderName || ''
    this.imageId = params.imageId || ''
    this.imageDate = params.imageDate || ''
    this.clipPolygons = params.clipPolygons || []
    this.clipArea = 100
  }

  getPlanetOrder(): PlanetOrder {
    let order: PlanetOrder = clone(planetOrderForm)
    order.name = this.orderName
    order.products[0].item_ids.push(this.imageId)
    this.clipPolygons.forEach(polygon => {
      order.tools.push(getClipTool(polygon))
    })
    return order
  }

  updateOrderName(value: string) {
    this.orderName = value
  }
}

export interface GeojsonOrder {
  type: string
  name: string
  features: CustomFeature[]
}

export interface CustomFeature {
  type: string
  properties: {
    aoi_name: string
    image_id: string
    image_date: string
  }
  geometry: any
}

export const planetOrderForm: PlanetOrder = {
  name: '',
  products: [
    {
      item_ids: [],
      item_type: 'PSScene4Band',
      product_bundle: 'analytic'
    }
  ],
  tools: []
}

export function getClipTool(polygon: Polygon): ClipTool {
  return {
    clip: {
      aoi: polygon
    }
  }
}
