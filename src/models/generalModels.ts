export interface PlanetOrder {
  name: string
  products: [
    {
      item_ids: string[]
      item_type: string
      product_bundle: string
    }
  ]
  tools: any[]
}

export interface ClipTool {
  clip: {
    aoi: Polygon
  }
}

export interface MultiPolygon {
  type: 'MultiPolygon'
  coordinates: [number, number][][]
}

export interface Polygon {
  type: 'Polygon'
  coordinates: [number, number][]
}
