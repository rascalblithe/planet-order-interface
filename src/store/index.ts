import Vue from 'vue'
import Vuex from 'vuex'
import { API_KEY } from '../utils/orderApi'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    defaultApiKey: API_KEY,
    defaultItemType: 'PSScene4Band',
    defaultProductBundle: 'visual'
  },
  mutations: {},
  actions: {},
  modules: {}
})
