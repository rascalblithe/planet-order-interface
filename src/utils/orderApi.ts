import { PlanetOrder } from '../models/generalModels'

const BASE_URL = 'https://api.planet.com/compute/ops'
export const API_KEY = '3838428973684e54935f18d5080581e7'

interface ApiRequest {
  url: string
  method: string
  headers?: {}
}

function request(options: ApiRequest) {
  const url = `${BASE_URL}${options.url}`
  options.headers = {
    'Content-Type': 'application/json',
    Authorization: `api-key ${API_KEY}`
  }
  return fetch(url, options)
}

export async function listOrders() {
  const query: ApiRequest = {
    url: '/orders/v2',
    method: 'GET'
  }
  return request(query)
    .then(res => {
      return res.json()
    })
    .then(data => {
      return data.orders
    })
}

export function getOrder(itemId: string) {
  const query: ApiRequest = {
    url: `/orders/v2/${itemId}`,
    method: 'GET'
  }
  return request(query)
    .then(res => {
      return res.json()
    })
    .then(data => {
      return data.orders
    })
}

export function cancelOrder(itemId: string) {
  const query: ApiRequest = {
    url: `/orders/v2/${itemId}`,
    method: 'PUT'
  }
  return request(query)
    .then(res => {
      return res.json()
    })
    .then(data => {
      return data.orders
    })
}

export function getOrderStats(itemId: string) {
  const query = {
    url: 'stats/orders/v2',
    method: 'GET'
  }
  return request(query).then(res => {
    return res.json()
  })
}

export function createOrder(order: PlanetOrder) {
  console.log('Ordering ' + order.name)
  const query = {
    url: 'stats/orders/v2',
    method: 'POST'
  }
  //   return request(query).then(res => {
  //     return res.json()
  //   })
  return true
}
