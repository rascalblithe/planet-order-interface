import { Polygon } from '../models/generalModels'
import { TimahOrder, GeojsonOrder } from '@/models/TimahOrderClass'

export function generateOrders(geojson: GeojsonOrder): TimahOrder[] {
  let data: TimahOrder[] = []
  geojson.features.forEach(feature => {
    const id = feature.properties.image_id

    const orderName =
      id.substring(0, 4) +
      '-' +
      id.substring(4, 6) +
      '@' +
      feature.properties.aoi_name

    let clipPolygons: Polygon[] = []

    if (feature.geometry.type == 'Polygon') {
      clipPolygons.push(buildPolygon(feature.geometry.coordinates[0]))
    } else if (feature.geometry.type == 'MultiPolygon') {
      feature.geometry.coordinates.forEach(
        (polygonGeom: [number, number][]) => {
          const geom = buildPolygon(polygonGeom)
          clipPolygons.push(geom)
        }
      )
    }

    data.push(
      new TimahOrder({
        orderName: orderName,
        imageId: id,
        imageDate: feature.properties.image_date,
        clipPolygons: clipPolygons
      })
    )
  })
  return data
}
export function buildPolygon(polygonGeom: [number, number][]): Polygon {
  return {
    type: 'Polygon',
    coordinates: polygonGeom
  }
}
